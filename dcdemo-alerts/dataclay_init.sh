#!/bin/bash
STARTUP_FILE=${DC_SHARED_VOLUME}/startup

while ! test -f $STARTUP_FILE; do
    echo "Waiting for $STARTUP_FILE to exist"
    sleep 30
done

export DATACLAYCLIENTCONFIG=${DC_SHARED_VOLUME}/client.properties
export DATACLAYSESSIONCONFIG=${DC_SHARED_VOLUME}/session.properties

echo "Starting main application"
exec python -u ./detect_obstacles.py "$@"
