import os
import time
from random import random
from datetime import datetime, timedelta
from uuid import uuid4

from dataclay.api import init, finish

# Configuration variables (per station)
LONGITUDE = float(os.getenv("LONGITUDE"))
LATITUDE = float(os.getenv("LATITUDE"))

SEND_TO_MQTT = bool(int(os.getenv("SEND_TO_MQTT")))
SEND_TO_KAFKA = bool(int(os.getenv("SEND_TO_KAFKA")))

if __name__=="__main__":
    print("*** Starting detect_obstacles.py DEMO")
    init()
    from CityNS.classes import Alert # Must be imported AFTER init

    i = 0
    while True:
        longitude = LONGITUDE + (random() - 0.5) * 0.008
        latitude = LATITUDE + (random() - 0.5) * 0.008

        from_ts = datetime.now()
        to_ts = from_ts + timedelta(seconds=10)

        # generate alert
        print("Generating alert #%d" % i)
        alert = Alert(id=str(uuid4().int), source="Pontevecchio:Camera:1", 
                      alert_category="hazardOnRoad", severity="high",
                      longitude=longitude, latitude=latitude, timestamp=from_ts,
                      valid_from=from_ts, valid_to=to_ts, area="pontevecchio",
                      description="There is an obstacle in the street",
                      data={"videoURL": "www.smartsecurity.com/video123.mp4",
                            "initialFrame": "80",
                            "finalFrame": "120"})
        alert.make_persistent()
        print(f"ALERT! Obstacle at [{longitude},{latitude}] ...")
        if SEND_TO_MQTT:
            print("Sending to MQTT...")
            alert.send_to_mqtt(topic="alerts")
        if SEND_TO_KAFKA:
            print("Sending to Kafka...")
            alert.send_to_kafka("dataclay", "resistenza")

        time.sleep(15)
        i = i + 1

    print("Obstacle detection finished!")
    print("Exiting Application!")
    finish() # Finish connection with dataClay

    
