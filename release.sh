#!/bin/bash
#===================================================================================
#
# FILE: release.sh
#
# USAGE: release.sh
#
# DESCRIPTION: Deploy mqtt to dataClay into DockerHub
#
# OPTIONS:
## ./release.sh
# REQUIREMENTS: ---
# BUGS: ---
# NOTES: ---
# AUTHORS: dgasull@bsc.es and cristovao.cordeiro@sixsq.com
# COMPANY: BSC and SixQ
# VERSION: 1.0
#===================================================================================

CONTAINERS=("mqtt-to-dataclay" "dataclay-connect" "dcdemo-alerts" "mqtt-to-mqtt-router")
PLATFORMS=linux/amd64,linux/arm64

#-----------------------------------------------------------------------
# MAIN
#-----------------------------------------------------------------------
set -e
bc -v

red=$'\e[1;91m'
end=$'\e[0m'

echo "$red Welcome to mqtt-to-dataClay deployment script $end"
read -r -p "$red Are you releasing a development version? [y/N] $end" response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]; then
  CUR_DATE=$(date -u +"%Y%m%d")
  TAG="dev${CUR_DATE}"
else
  echo "$red Which tag do you want to release?"
  read TAG
fi
echo "$red Going to publish: $end"
for container_name in "${CONTAINERS[@]}" ; do
	echo "    $red- elasticeuh2020/${container_name}:${TAG} $end"
done
read -r -p "$red Are you sure? [y/N] $end" response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]; then

  SECONDS=0

  ############################# Push into DockerHub #############################
  for container_name in "${CONTAINERS[@]}" ; do

	  echo " ===== Building elasticeuh2020/${container_name}:${TAG} ====="
	pushd ${container_name}
  docker buildx build --rm -t elasticeuh2020/${container_name}:$TAG \
    --cache-to=type=registry,ref=elasticeuh2020/${container_name}:buildxcache,mode=max \
    --cache-from=type=registry,ref=elasticeuh2020/${container_name}:buildxcache \
    --platform $PLATFORMS --push .
  popd
done


  #######################################################################################

  duration=$SECONDS
  echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
  echo "Deployment FINISHED! "

  echo "  ==  Everything seems to be ok!"
  echo ""
  echo ""
  echo "You may want to run the following:"
  echo ""
  echo "git tag ${TAG}"
  echo "git push --tags"

else
  echo "Aborting"
fi
