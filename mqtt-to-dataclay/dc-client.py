#!/usr/bin/env python
from distutils.util import strtobool
import os
import json

from mqtt_fiware_bridge import MFB
from dataclay.api import init, finish, register_dataclay, get_external_backend_id_by_name
from dataclay.exceptions.exceptions import DataClayException

MY_STATION = os.getenv("MY_STATION", "resistenza")
SEND_TO_KAFKA = strtobool(os.getenv("SEND_TO_KAFKA", "False"))

# Init performed before importing registered classes
init()

from CityNS.classes import Event, DKB



class MQTTtoDataClayBridge(MFB.MqttFiwareBridge):

    def __init__(self, kb, **kwargs):
        super(MQTTtoDataClayBridge, self).__init__(**kwargs)
        self.kb = kb
        self.tram_presence_flags = [False, False]

    @staticmethod
    def direction_and_station(msg_json):
        # We are in the cloud, and we are gonna check the station
        # (currently only Resistenza is implemented)
        # Direction to Scandicci: 10492.202 < d < 10565.720
        # Direction to Florence: 1113.485 < d < 1178.324

        spline_id = msg_json["spline_id"]
        arc = msg_json["arc_length_from_origin_m"]

        if spline_id == 111:
            # Florence direction
            if (1113.485 < arc) and (arc < 1178.324):
                return 0, "resistenza"

            return 0, None
        elif spline_id == 121:
            # Scandicci direction
            if (10492.202 < arc) and (arc < 10565.720):
                return 1, "resistenza"
            
            return 1, None
        else:
            print("Unknown spline_id=%d, ignoring" % msg_json["spline_id"])
            return -1, None

    def send_to_dataclay(self, msg_json, topic):
        event = None
        if topic == "/Thales/NGAP_SFA_Output":
            self.log.info("Creating tram event")

            direction, station = self.direction_and_station(msg_json)
            self.tram_presence_flags[direction] = (station == MY_STATION)

            if station == MY_STATION:
                print("Tram is on my station (%s), creating Event" % MY_STATION)
                event = Event()
                event.make_persistent()
                event.init_from_fiware_vehicle(msg_json, self.kb)
                if SEND_TO_KAFKA:
                    self.log.info("Sending event to Kafka")
                    event.send_to_kafka("dataclay", MY_STATION)
                self.log.info("dataClay tram event created: " + str(event))
            else:
                print("Not my station, ignoring")
            
            if any(self.tram_presence_flags) and not self.kb.tram_in_station:
                print("New tram in the station")
                self.kb.tram_arriving()
            elif all(not flag for flag in self.tram_presence_flags) and self.kb.tram_in_station:
                print("No trams in the station")
                self.kb.tram_departing()
            else:
                print("No changes on station occupation")

        elif topic == "/Thales/ADAS_SFA_Output":
            self.log.info("Handing over creation to the DKB object")
            self.kb.add_events_from_json_adas(msg_json)

        elif topic == "TLRM":
            station = msg_json["TLRMId"].lower()

            if station == MY_STATION:
                self.log.info("Updating traffic lights status")
                tl = self.kb.update_traffic_light_status(msg_json)
                if SEND_TO_KAFKA:
                    try:
                        self.log.info("Sending traffic light status to Kafka")
                        tl.send_to_kafka("dataclay", MY_STATION)
                    except:
                        pass
                self.log.info("dataClay traffic lights status updated: " + str(tl))
            else:
                print("TrafficLight update was not for this station")

    def do_something(self, message):
        raise NotImplementedError("This method should not be called by anyone, there is a bug in the code")

    def on_message(self, client, userdata, message):
        # This is reimplemented by looking into the original code
        # and extracting the topic too (do_something is now unused)

        # note that fiware_validation is ignored ATM
        self.log.info(f"New message: {message.payload}")
        self.log.info(f"On topic: {message.topic}")

        msg_json = json.loads(message.payload)
        self.send_to_dataclay(msg_json, message.topic)


if __name__ == "__main__":
    # write state file for health check
    try:
        f = open("/srv/dataclay/shared/mqtt_to_dataclay_state.txt", "w")
        f.write("READY")
        f.close()
    except:
        print("State file not writable. Skipping file creation.")

    # Check that DKB exists and create it if needed
    try:
        kb = DKB.get_by_alias("DKB")
    except DataClayException as e:
        kb = DKB()
        try:
            kb.make_persistent(alias="DKB")
        except DataClayException:
            pass

    # import to check stubs were found
    print("  =====   Ready to start bridging MQTT to DataClay")

    bridge = MQTTtoDataClayBridge(kb)
    # ADAS message should not be validated
    bridge.args.ignore_fiware_validation = True
    bridge.connect()
    # endlesss loop

    print("Exiting Application...")
    finish() # Finish connection with dataClay
