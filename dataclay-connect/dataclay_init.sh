#!/bin/bash
set -x
set -e

CONTRACT_ID_FILE=${DC_SHARED_VOLUME}/${NAMESPACE}_contractid

export DATACLAYCLIENTCONFIG=${DC_SHARED_VOLUME}/client.properties
export DATACLAYSESSIONCONFIG=${DC_SHARED_VOLUME}/session.properties
export STUBSPATH=${DC_SHARED_VOLUME}/stubs
export STARTUP_FILE=${DC_SHARED_VOLUME}/startup

### Prepare startup file
rm -f ${STARTUP_FILE}

########################### create cfgfiles ###########################

printf "HOST=${LOGICMODULE_HOST}\nTCPPORT=${LOGICMODULE_PORT_TCP}" > ${DATACLAYCLIENTCONFIG}
echo "Account=${USER}
Password=${PASS}
DataSets=${DATASET}
DataSetForStore=${DATASET}
StubsClasspath=${STUBSPATH}" > ${DATACLAYSESSIONCONFIG}

######################################################

dataclay-java-entry-point es.bsc.dataclay.tool.AccessNamespace $USER $PASS $NAMESPACE | tail -1 > ${CONTRACT_ID_FILE}

echo "Contract ID stored in ${CONTRACT_ID_FILE}. Contents:"
cat ${CONTRACT_ID_FILE}

export CONTRACT_ID=`cat ${CONTRACT_ID_FILE}`
mkdir -p ${STUBSPATH}
python -c "from dataclay.tool.functions import get_stubs; get_stubs(\"${USER}\", \"${PASS}\", \"${CONTRACT_ID}\", \"${STUBSPATH}\")"
echo "Stubs properly obtained!"

touch ${STARTUP_FILE}
