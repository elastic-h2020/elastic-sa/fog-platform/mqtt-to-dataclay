# dataClay containers

This repository contains various Dockerfile that are used for the ELASTIC project.

The script `release.sh` will build the images and update those in Docker Hub.

Note that the build process includes several architectures (amd64 and arm64) and a caching mechanism is used
(through the _buildxcache_ tag name) in order to speed up the process.

The registered model used in the project is not found here; instead you can follow its development here:

https://gitlab.bsc.es/ppc-bsc/software/dataclay-object-detection-model.git

## Build errors (buildx fails)

RTFM at https://docs.docker.com/buildx/working-with-buildx/

Probably, just run

```
$ docker run --privileged --rm tonistiigi/binfmt --install all
```

If you have messed up your environment, a way to clean it up seems to be:

```
$ docker run --privileged --rm tonistiigi/binfmt --uninstall qemu-*
```

But that should not be necessary in most cases.

## Acknowledgements
This work has been supported by the EU H2020 project ELASTIC, contract #825473.
