#!/usr/bin/env python
import socket
import paho.mqtt.client as mqtt
import json


# Both for receiving (in the cloud) and sending (into each station)
TOPIC = "/Thales/NGAP_SFA_Output"

ARC_MARGIN = 70

STATION_IP_MAPPING = {
    "resistenza": "192.168.121.246",
#    "arcipressi": "192.168.121.0",
#    "batoni": "192.168.121.0"
}

STATION_CLIENT_MAPPING = {}


# Cloud (local, because the router is also in the cloud) IP
CONSUMER_IP = "192.168.170.103"
consumer = None

def get_stations_for_ngap_message(msg_dict):
    # We are in the cloud, and we are gonna check the station
    # (currently only Resistenza is implemented)
    # Direction to Scandicci: 10492.202 < d < 10565.720
    # Direction to Florence: 1113.485 < d < 1178.324

    spline_id = msg_dict["spline_id"]
    arc = msg_dict["arc_length_from_origin_m"]

    clients = list()

    if spline_id == 111:
        # Florence direction
        if (1113.485 - ARC_MARGIN < arc) and (arc < 1178.324 + ARC_MARGIN):
            clients.append("resistenza")

    elif spline_id == 121:
        # Scandicci direction
        if (10492.202 - ARC_MARGIN < arc) and (arc < 10565.720 + ARC_MARGIN):
            clients.append("resistenza")
    
    else:
        print("Unknown spline_id=%d, ignoring" % msg_dict["spline_id"])
        return 

    return clients


def on_connect_with_subscribe(client, userdata, flags, rc):
    print("A consumer connection established, proceeding to subscribe")
    client.subscribe(TOPIC)


def on_connect_plain(client, userdata, flags, rc):
    print("Plain connection established")
    client.subscribe(TOPIC)


def on_disconnect(self, client, userdata, rc):
    print('Disconnect result {}'.format(rc))


def on_message(client, userdata, msg_raw):
    msg = json.loads(msg_raw.payload)

    for station in get_stations_for_ngap_message(msg):
        print("Publishing message to station %s" % station)
        client = STATION_CLIENT_MAPPING[station]
        client.publish(TOPIC, msg_raw.payload)


if __name__ == "__main__":
    print("Preparing producer clients for each station:")

    for station, mqtt_host in STATION_IP_MAPPING.items():
        print(" ... trying %s station at %s" % (station, mqtt_host))
        client = mqtt.Client(client_id="mqtt-router")
        client.on_connect = on_connect_plain
        client.on_disconnect = on_disconnect

        try:
            client.connect(mqtt_host, 1883, 60)
        except socket.timeout:
            print("Station %s was unresponsive (socket timed out). Skipping" % station)

        STATION_CLIENT_MAPPING[station] = client

    print("Preparing consumer client (for data received in the cloud)")
    consumer = mqtt.Client(client_id="mqtt-router")
    consumer.on_connect = on_connect_with_subscribe
    consumer.on_message = on_message
    consumer.on_disconnect = on_disconnect
    consumer.connect(CONSUMER_IP, 1883, 60)

    print("Going into main loop")
    # let's loop all the MQTT clients (nothing else to do here)
    while True:
        consumer.loop()
        for client in STATION_CLIENT_MAPPING.values():
            client.loop()
