#!/bin/bash
#===================================================================================

CONTAINERS=("mqtt-to-dataclay" "dataclay-connect-multidc" "dataclay-connect" "dcdemo-alerts")

#-----------------------------------------------------------------------
# MAIN
#-----------------------------------------------------------------------

red=$'\e[1;91m'
end=$'\e[0m'

echo "$red Welcome to mqtt-to-dataClay deployment script $end"
read -r -p "$red Are you releasing a development version? [y/N] $end" response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]; then
  CUR_DATE=$(date -u +"%Y%m%d")
  TAG="dev${CUR_DATE}"
else
  echo "$red Which tag do you want to release?"
  read TAG
fi
echo "$red Going to publish: $end"
for container_name in "${CONTAINERS[@]}" ; do
	echo "    $red- elasticeuh2020/${container_name}:${TAG} $end"
done
read -r -p "$red Are you sure? [y/N] $end" response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]; then

  SECONDS=0

  ############################# Push into DockerHub #############################
  echo "$red Login into DockerHub with elastic user $end"

  for container_name in "${CONTAINERS[@]}" ; do

	  echo " ===== Building elasticeuh2020/${container_name}:${TAG} ====="
	pushd ${container_name}
  docker build --rm -t elasticeuh2020/${container_name}:$TAG .
  docker push elasticeuh2020/${container_name}:$TAG
  popd
done


  #######################################################################################

  duration=$SECONDS
  echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
  echo "Deployment FINISHED! "

  echo "  ==  Everything seems to be ok!"
  echo ""
  echo ""
  echo "You may want to run the following:"
  echo ""
  echo "git tag -a ${TAG}"
  echo "git push origin ${TAG}"

else
  echo "Aborting"
fi
